
extends CanvasLayer

onready var daytime_filter = get_node("daytime_filter")
onready var overlay = get_node("overlay")
onready var time_display = get_node("time_display")
onready var still_texture = get_node("still_texture")
onready var dialogue_box = get_node("dialogue_box")
onready var choice_box = get_node("choice_box")
onready var pause_menu = get_node("pause_menu")
