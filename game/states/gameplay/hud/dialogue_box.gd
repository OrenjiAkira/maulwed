extends Node2D

signal dialogue_ended
signal dialogue_started

enum STATE { IDLE, ENTER, TEXT_ANIMATION, AWAIT_CONFIRM, LEAVE }

const Pack = preload("res://common/pack.gd")

const SPEED = 60
const SCROLL_SLOW = 1
const SCROLL_FAST = 2
const FADE_TIME = 0.2
const OPAQUE = Color(0xffffffff)
const TRANSP = Color(0xffffff00)
const BOTTOM_Y = 256
const MIDDLE_Y = 128
const ONSCREEN_X = 0
const OFFSCREEN_X = 640

# elements
onready var panel = get_node("panel")
onready var text = get_node("text")
onready var punctuation = get_node("punctuation")
onready var speaker = get_node("speaker")
onready var tween_in = get_node("tween_in")
onready var tween_out = get_node("tween_out")

# state values
var buffer = []
var state = STATE.IDLE
var text_animation = { "step": 0, "acc": SCROLL_SLOW }
var allow_skip = true

func _ready():
	hide()

func _process(dt):
	if is_state(STATE.ENTER):
		setup_text()
		hide_punctuation()
		set_state(STATE.TEXT_ANIMATION)
	elif is_state(STATE.LEAVE):
		if buffer.size() == 0:
			stop()
			set_state(STATE.IDLE)
		else:
			# animate arrow here
			set_state(STATE.ENTER)
	elif is_state(STATE.TEXT_ANIMATION):
		var percent_visible = text.get_percent_visible()
		if percent_visible < 1:
			var step = text_animation.step
			var acc = text_animation.acc
			text.set_percent_visible(min(1, percent_visible + SPEED * step * acc * dt))
			text_animation.acc = SCROLL_SLOW
		else:
			set_state(STATE.AWAIT_CONFIRM)

# Setup

func setup(text_string):
	buffer = text_string.split("\n\n", false)
	buffer.invert()
	text.clear()
	hide_punctuation()
	enter_animation()
	show()
	yield(tween_in, "tween_completed")
	set_state(STATE.ENTER)
	emit_signal("dialogue_started")

func stop():
	text.clear()
	hide_punctuation()
	leave_animation()
	yield(tween_out, "tween_completed")
	hide()
	emit_signal("dialogue_ended")

func setup_position(centered):
	if centered:
		position.y = MIDDLE_Y
	else:
		position.y = BOTTOM_Y

func setup_opacity(transparent):
	if transparent:
		panel.set_modulate(TRANSP)
	else:
		panel.set_modulate(OPAQUE)

func setup_text():
	var idx = buffer.size() - 1
	var text_line = buffer[idx]
	buffer.remove(idx)
	text.clear()
	text.append_bbcode(text_line)
	text.set_percent_visible(0)
	text_animation.acc = SCROLL_SLOW
	text_animation.step = 1.0 / text_line.length()

# Animations

func enter_animation():
	speaker.position = Vector2(-OFFSCREEN_X, speaker.position.y)
	panel.rect_position = Vector2(OFFSCREEN_X, panel.rect_position.y)
	tween_in.interpolate_property(speaker, "position",
			Vector2(-OFFSCREEN_X, speaker.position.y),
			Vector2(ONSCREEN_X, speaker.position.y), FADE_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_in.interpolate_property(panel, "rect_position",
			Vector2(OFFSCREEN_X, panel.rect_position.y),
			Vector2(ONSCREEN_X, panel.rect_position.y), FADE_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_in.start()

func leave_animation():
	tween_out.interpolate_property(speaker, "position",
			Vector2(ONSCREEN_X, speaker.position.y),
			Vector2(-OFFSCREEN_X, speaker.position.y), FADE_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_out.interpolate_property(panel, "rect_position",
			Vector2(ONSCREEN_X, panel.rect_position.y),
			Vector2(OFFSCREEN_X, panel.rect_position.y), FADE_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_out.start()

# Changing state

func set_state(st):
	state = st

func is_state(st):
	return state == st

func set_allow_skip(allow):
	allow_skip = allow

func accelerate():
	if allow_skip:
		text_animation.acc = SCROLL_FAST

func skip():
	if allow_skip:
		text.set_percent_visible(1)

func next():
	if is_state(STATE.AWAIT_CONFIRM):
		set_state(STATE.LEAVE)

func show_punctuation():
	if !punctuation.visible:
		punctuation.show()

func hide_punctuation():
	if punctuation.visible:
		punctuation.hide()

func set_speaker(speaker_name):
	speaker.show()
	speaker.set_name(speaker_name)

func unset_speaker():
	speaker.hide()

func is_buffer_empty():
	return buffer.size() == 0
