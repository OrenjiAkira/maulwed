
extends Polygon2D

const RADIUS = 23.5

onready var bg = get_node("bg")

var vertex = [] setget ,get_vertex

func _ready():
	vertex.append(Vector2(0, -1)*RADIUS)
	for i in range(60):
		var angle = (i+1) * PI/30 - PI/2
		vertex.append(RADIUS * Vector2(cos(angle), sin(angle)))
	set_polygon(get_vertex())
	var bg_vertex = get_vertex()
	for i in range(bg_vertex.size()):
		bg_vertex[i] *= 1/RADIUS * (RADIUS+1)
	bg.set_polygon(bg_vertex)

func get_vertex():
	return vertex.duplicate()

func set_minutes(m):
	var vlist = get_vertex()
	if m > 0:
		for i in range(0, m):
			vlist[i] = Vector2(0, 0)
	set_polygon(vlist)
