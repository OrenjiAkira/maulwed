
extends Node2D

signal fade_done
signal clear_done

const FADE_TIME = 2.0
const PACK = preload("res://common/pack.gd")

var stills = PACK.new("domains/stills")
var timer = Timer.new()
var clear_timer = Timer.new()
var current

func _enter_tree():
	add_child(timer)
	add_child(clear_timer)

func _exit_tree():
	timer.free()
	stills.free()

func set_still(still_name, fade_time):
	var still = stills.instance(still_name)
	clear_still(fade_time)
	add_child(still)
	current = still
	still.fade_in(fade_time)
	timer.set_wait_time(fade_time)
	timer.start()
	yield(timer, "timeout")
	emit_signal("fade_done")

func clear_still(fade_time):
	var still = current
	if current:
		current.fade_out(fade_time)
	current = null
	clear_timer.set_wait_time(fade_time)
	clear_timer.start()
	yield(clear_timer, "timeout")
	emit_signal("clear_done")
	