extends Node2D

const SAVE_MENU = preload("res://common/save_menu.tscn")

const TRANSP = Color(0xffffff00)
const OPAQUE = Color(0xffffffff)
const ENTER_TIME = 0.25

enum { OPTION_RESUME, OPTION_LOAD, OPTION_SAVE, OPTION_QUIT }
enum { STATUS_RESUME, STATUS_GAMEQUIT }

signal pause_exited

onready var overlay = get_node("overlay")
onready var root_menu = get_node("menu")
onready var tween = get_node("tween")


func _ready():
	hide()
	set_modulate(TRANSP)
	root_menu.connect("pressed_A", self, "execute_action")
	root_menu.connect("pressed_B", self, "execute_quit")
	root_menu.connect("pressed_pause", self, "execute_quit")

func enter():
	get_tree().paused = true
	show()
	root_menu.se.play("confirm")
	root_menu.setup()
	tween.interpolate_property(self, "modulate", TRANSP, OPAQUE,
			ENTER_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	root_menu.focus()

func leave(cancel):
	root_menu.unfocus()
	if cancel:
		se.play("cancel")
	tween.interpolate_property(self, "modulate", OPAQUE, TRANSP,
			ENTER_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	hide()
	get_tree().paused = false
	emit_signal("pause_exited", STATUS_RESUME)

func execute_action(action):
	var se = root_menu.se
	var save_menu
	var save_menu_mode
	se.play("confirm")
	match action:
		OPTION_RESUME:
			return leave(false)
		OPTION_LOAD:
			save_menu = SAVE_MENU.instance()
			save_menu_mode = save_menu.LOAD_MODE
		OPTION_SAVE:
			save_menu = SAVE_MENU.instance()
			save_menu_mode = save_menu.SAVE_MODE
		OPTION_QUIT:
			root_menu.unfocus()
			return emit_signal("pause_exited", STATUS_GAMEQUIT)
	# save and or load
	root_menu.unfocus()
	tween.interpolate_property(root_menu, "modulate", OPAQUE, TRANSP,
			ENTER_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	root_menu.hide()
	add_child(save_menu)
	save_menu.enter(save_menu_mode)
	yield(save_menu, "tree_exiting")
	root_menu.setup()
	root_menu.show()
	tween.interpolate_property(root_menu, "modulate", TRANSP, OPAQUE,
			ENTER_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	yield(tween, "tween_completed")
	root_menu.focus()

func execute_quit(idx):
	leave(true)
