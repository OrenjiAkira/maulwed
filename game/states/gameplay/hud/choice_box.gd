
extends Node2D

const FADE_TIME = 0.2
const ONSCREEN_X = 0
const OFFSCREEN_X = 640
const LH = 32

const COLOR_SELECTED = Color(0xffffffff)
const COLOR_UNSELECTED = Color(0x606060ff)

signal choice_entered
signal choice_left

onready var question = get_node("question")
onready var question_text = question.get_node("question_text")
onready var panel = get_node("panel")
onready var options = get_node("options").get_children()
onready var selection = get_node("selection")
onready var initial_pos = selection.position
onready var tween_in = get_node("tween_in")
onready var tween_out = get_node("tween_out")

func _ready():
	hide()

func setup(question_string, options_array):
	for i in range(2):
		options[i].set_text(options_array[i])
		options[i].hide()
	question_text.hide()
	selection.hide()
	set_selection(0)
	panel.rect_position = Vector2(OFFSCREEN_X, panel.rect_position.y)
	if !question_string.empty():
		question.get_node("question_text").set_text(question_string)
		question.position = Vector2(-OFFSCREEN_X, question.position.y)
		question.show()
		tween_in.interpolate_property(question, "position",
				Vector2(-OFFSCREEN_X, question.position.y),
				Vector2(ONSCREEN_X, question.position.y),
				FADE_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	else:
		question.hide()
	tween_in.interpolate_property(panel, "rect_position",
			Vector2(OFFSCREEN_X, panel.rect_position.y),
			Vector2(ONSCREEN_X, panel.rect_position.y),
			FADE_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_in.start()
	show()
	yield(tween_in, "tween_completed")
	for i in range(2):
		options[i].show()
	question_text.show()
	selection.show()
	emit_signal("choice_entered")

func stop():
	for i in range(2):
		options[i].hide()
	question_text.hide()
	selection.hide()
	tween_out.interpolate_property(question, "position",
			Vector2(ONSCREEN_X, question.position.y),
			Vector2(-OFFSCREEN_X, question.position.y),
			FADE_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_out.interpolate_property(panel, "position",
			Vector2(ONSCREEN_X, panel.rect_position.y),
			Vector2(OFFSCREEN_X, panel.rect_position.y),
			FADE_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween_out.start()
	yield(tween_out, "tween_completed")
	hide()
	emit_signal("choice_left")

func set_selection(idx):
	assert(idx >= 0 and idx < 2)
	selection.set_position(initial_pos + Vector2(0, idx * LH))
	options[idx].set_self_modulate(COLOR_SELECTED)
	options[idx ^ 1].set_self_modulate(COLOR_UNSELECTED)

