
extends ColorRect

const DEFS = preload("res://domains/definitions.gd")
const ENTER_TIME = 0.4
const TRANSP = Color(0xffffff00)
const OPAQUE = Color(0xffffffff)

static func parse(time):
	time = int(time)
	var h = time / 3600 % 24
	var m = time / 60 % 60
	var period = "AM"
	if h > 12:
		period = "PM"
	h = h % 12
	if h == 0:
		h = 12
	return "%02d:%02d %s" % [h, m, period]

onready var gamedata = get_node("/root/profile").current_save
onready var time_text = get_node("time_text")
onready var ampm = get_node("ampm")
onready var roundcounter = get_node("roundcounter")
onready var tween = get_node("tween")
onready var enter_position = rect_position * 1
onready var enter_distance = Vector2(rect_size.x*2, 0)

var t = 0
var enabled = false

func _ready():
	rect_position += enter_distance

func _physics_process(dt):
	if enabled:
		t += dt * DEFS.SECOND_PER_SECOND * gamedata.get_persist("game_speed")
		save_time()

func load_time():
	t = gamedata.get_state("time")
	update_time_display()

func save_time():
	update_time_display()
	gamedata.set_state("time", int(t))

func update_time_display():
	var h = int(t/60/60)
	var m = int(t/60) % 60
	if h % 24 >= 12:
		ampm.set_text("PM")
	else:
		ampm.set_text("AM")
	h = h % 12
	if h == 0:
		h = 12
	time_text.set_text(String(h))
	roundcounter.set_minutes(m)

func start(allowed):
	if not allowed and enabled:
		stop()
	elif allowed and not enabled:
		load_time()
		enabled = true
		tween.stop_all()
		tween.interpolate_property(self, "modulate", modulate, OPAQUE, ENTER_TIME,
				Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
		tween.interpolate_property(self, "rect_position",
				rect_position, enter_position, ENTER_TIME,
				Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
		tween.start()

func stop():
	if enabled:
		enabled = false
		tween.stop_all()
		tween.interpolate_property(self, "modulate", modulate, TRANSP, ENTER_TIME,
				Tween.TRANS_QUAD, Tween.EASE_IN, 0)
		tween.interpolate_property(self, "rect_position",
				rect_position, enter_position + enter_distance, ENTER_TIME,
				Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
		tween.start()

