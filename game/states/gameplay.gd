
# GAMEPLAY STATE
extends "res://infra/gamestate.gd"

const PACK = preload("res://common/pack.gd")

enum { EXIT_OK, EXIT_QUIT }
const INITIAL_SPAWN_NAME = "initial"

export (String) var current_map_id

onready var hud = get_node("hud")
onready var debug = get_node("debug")
onready var gamedata = get_node("/root/profile").current_save
onready var fader = get_node("/root/main/effects/fader")

var maps = PACK.new("domains/maps")
var current_map

func _ready():
	gamedata.connect("game_saving", self, "save_current_map")
	init_map()

func _exit_tree():
	maps.free()

func enter():
	pass

func leave():
	current_map.pause()

func init_map():
	var map_info = gamedata.get_current_map()
	var map_id = map_info.map_id
	var spawn = map_info.spawn
	if !map_id:
		map_id = current_map_id
		spawn = INITIAL_SPAWN_NAME
	load_map(map_id, spawn)

func load_map(map_id, spawn):
	var map = maps.instance(map_id)
	add_child(map)
	current_map = map
	current_map_id = map_id
	gamedata.load_map(map_id, map)
	gamedata.set_current_map(map_id, map.get_map_name(), spawn)
	setup_time_display(map)
	setup_map_scenes(map)
	setup_map_quit_event(map)
	map.enter(hud, spawn)

func setup_map_quit_event(map):
	map.connect("map_changed", self, "change_map", [], CONNECT_ONESHOT)
	map.connect("requested_quit", self, "emit_signal",
			["requested_state_switch", "start"], CONNECT_ONESHOT)

func setup_time_display(map):
	var time_display = hud.time_display
	time_display.load_time()
	map.connect("map_resumed", time_display, "start", [map.does_time_flow()])
	map.connect("map_paused", time_display, "stop")

func setup_map_scenes(map):
	# setup map's global scene managers
	map.scenes.setup(map, hud)
	map.behaviours.setup(map, hud)
	# setup each body's scene manager
	for body in map.get_bodies():
		var scene_manager = body.get_scenes()
		if scene_manager != null:
			scene_manager.setup(map, hud)

func unload_map():
	var map = current_map
	if map != null:
		map.leave()
		gamedata.save_map(current_map_id, map)
		map.disconnect("requested_quit", self, "emit_signal")
		map.free()
		current_map = null

func change_map(map_id, spawn):
	fader.fade_out(.4)
	yield(fader, "faded_out")
	unload_map()
	# wait one frame, just because
	yield(get_tree(), "physics_frame")
	# carry on
	load_map(map_id, spawn)
	fader.fade_in(.4)
	yield(fader, "faded_in")

func save_current_map():
	if current_map:
		gamedata.save_map(current_map_id, current_map)


# GAMEPLAY INPUT

func _physics_process(dt):
	if not current_map or current_map.paused:
		return
	var player = current_map.get_player()
	var direction = player.DIR_NEUTRAL
	if Input.is_action_pressed("DIR_RIGHT"):
		direction = player.DIR_RIGHT
	elif Input.is_action_pressed("DIR_LEFT"):
		direction = player.DIR_LEFT
	player.walk(direction)

func _input(ev):
	if not current_map or current_map.paused:
		return
	if ev.is_action_pressed("META_DEBUG"):
		open_debug()
		yield(debug, "debug_done")
		close_debug()
	elif ev.is_action_pressed("ACTION_PAUSE"):
		open_pause_menu()
		close_pause_menu(yield(hud.pause_menu, "pause_exited"))
	elif ev.is_action_pressed("ACTION_A"):
		current_map.check_interact_trigger()

func open_debug():
	current_map.pause()
	debug.enter()

func close_debug():
	current_map.run()

func open_pause_menu():
	current_map.pause()
	hud.pause_menu.enter()

func close_pause_menu(exit_status):
	match exit_status:
		EXIT_OK:
			print("Exited menu with status 0 (resume game)")
			current_map.run()
		EXIT_QUIT:
			print("Exited menu with status 1 (game quit)")
			emit_signal("requested_state_switch", "start")
