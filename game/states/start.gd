# MENU GAMESTATE
extends "res://infra/gamestate.gd"

enum { OPTION_NEW, OPTION_LOAD, OPTION_SETTINGS, OPTION_QUIT }

const SAVE_MENU = preload("res://common/save_menu.tscn")

# root nodes
onready var music = get_node("/root/music")
onready var se = get_node("/root/se")
onready var profile = get_node("/root/profile")

# local attributes
export(AudioStream) var start_bgm

onready var root_menu = get_node("menu")

func _ready():
	load_music()
	root_menu.connect("pressed_A", self, "execute_action")
	root_menu.setup()
	root_menu.disable(2)
	if profile.has_saves():
		root_menu.set_selection(1)
	root_menu.focus()

func load_music():
	if start_bgm:
		music.play(start_bgm, 0.5)
	else:
		music.stop(0.5)

func leave():
	music.stop(0.5)

func execute_action(idx):
	match idx:
		OPTION_NEW:
			se.play("confirm")
			profile.new_game()
			root_menu.unfocus()
			emit_signal("requested_state_switch", "gameplay")
		OPTION_LOAD:
			se.play("confirm")
			root_menu.unfocus()
			root_menu.fade_out()
			yield(root_menu, "faded_out")
			var save_menu = SAVE_MENU.instance()
			add_child(save_menu)
			save_menu.enter(save_menu.LOAD_MODE)
			yield(save_menu, "tree_exiting")
			root_menu.setup()
			root_menu.fade_in()
			yield(root_menu, "faded_in")
			root_menu.focus()
		OPTION_SETTINGS:
			se.play("forbidden")
		OPTION_QUIT:
			se.play("confirm")
			emit_signal("requested_game_quit")
