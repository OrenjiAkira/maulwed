
extends Node2D

enum { GO_BACK, TOGGLE_FULLSCREEN, TOGGLE_ASPECT }

const ASPECT_TEXT = "Aspect x%d"

onready var resolution = get_node("/root/resolution")
onready var menu = get_node("menu")

func _ready():
	menu.set_faded(true)
	menu.setup()
	menu.fade_in()
	yield(menu, "faded_in")
	menu.connect("pressed_A", self, "execute_confirm")
	menu.focus()

func leave():
	menu.unfocus()
	menu.fade_out()
	yield(menu, "faded_out")
	queue_free()

func execute_confirm(idx):
	match idx:
		GO_BACK:
			menu.se.play("cancel")
			leave()
		TOGGLE_FULLSCREEN:
			menu.se.play("confirm")
			resolution.toggle_fullscreen()
			if resolution.is_fullscreen():
				menu.get_option(idx).set_text("Window Mode")
			else:
				menu.get_option(idx).set_text("Fullscreen Mode")
		TOGGLE_ASPECT:
			menu.se.play("confirm")
			var new_aspect = resolution.get_aspect() ^ 3
			menu.get_option(idx).set_text(ASPECT_TEXT % new_aspect)
			resolution.set_aspect(new_aspect)
