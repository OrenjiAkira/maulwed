shader_type canvas_item;

uniform float CORNER = 8.0;
uniform vec2 QPOS = vec2(0, 0);
uniform vec2 QUAD = vec2(160, 160);

float dist(vec2 u, vec2 v) {
	return max(abs(u.x - v.x), abs(u.y - v.y));
}

void fragment() {
	vec2 tex_size = vec2(textureSize(TEXTURE, 0));
	vec2 lu = QPOS;
	vec2 ru = QPOS + vec2(QUAD.x, 0);
	vec2 ld = QPOS + vec2(0, QUAD.y);
	vec2 rd = QPOS + QUAD;
	vec2 pos = QPOS + UV * tex_size;
	float visible = step(CORNER, dist(lu, pos));
	visible = min(visible, step(CORNER, dist(ld, pos)));
	visible = min(visible, step(CORNER, dist(ru, pos)));
	visible = min(visible, step(CORNER, dist(rd, pos)));
	vec4 pixel_color = texture(TEXTURE, QPOS/tex_size+UV);
	COLOR = pixel_color * vec4(1, 1, 1, visible);
}
