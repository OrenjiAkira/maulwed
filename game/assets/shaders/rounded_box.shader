shader_type canvas_item;

uniform float WIDTH;
uniform float HEIGHT;
uniform float RAD;

void fragment() {
	float smoother = 0.0;

	vec2 size = vec2(WIDTH, HEIGHT);
	vec2 lu = vec2(RAD, RAD);
	vec2 ru = vec2(WIDTH-RAD, RAD);
	vec2 ld = vec2(RAD, HEIGHT-RAD);
	vec2 rd = size - lu;

	float visible = 1.0;
	visible = smoothstep(RAD, length(lu - UV), smoother);
	visible = max(visible, smoothstep(RAD, length(ru - UV), smoother));
	visible = max(visible, smoothstep(RAD, length(ld - UV), smoother));
	visible = max(visible, smoothstep(RAD, length(rd - UV), smoother));
	visible = max(visible, min(smoothstep(UV.x, RAD, smoother), smoothstep(WIDTH-RAD, UV.x, smoother)));
	visible = max(visible, min(smoothstep(UV.y, RAD, smoother), smoothstep(HEIGHT-RAD, UV.y, smoother)));

	COLOR = COLOR * vec4(1, 1, 1, visible);
}
