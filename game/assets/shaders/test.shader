shader_type canvas_item;

uniform vec2 DIM = vec2(160, 160);
uniform float CORNER = 8.0;

float corner_step(float a, vec2 u, vec2 v) {
	float dist = max(abs(u.x - v.x), abs(u.y - v.y));
	return min(a, step(CORNER, dist));
}

void fragment() {
	float visible = 1.0;
	vec2 pos = (UV * DIM);
	visible = corner_step(visible, pos, vec2(0, 0));
	visible = corner_step(visible, pos, vec2(DIM.x, 0));
	visible = corner_step(visible, pos, vec2(0, DIM.y));
	visible = corner_step(visible, pos, DIM);
	COLOR = COLOR * vec4(1, 1, 1, visible);
}
