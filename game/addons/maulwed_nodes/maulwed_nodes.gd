tool
extends EditorPlugin

func _enter_tree():
    # Initialization of the plugin goes here
    # Add the new type with a name, a parent type, a script and an icon
    add_custom_type("SpawnPoint", "Position2D",
            preload("spawn_point/script.gd"),
            preload("spawn_point/icon.png"))
    add_custom_type("BodyAnimations", "AnimationPlayer",
            preload("body_animations/script.gd"),
            preload("body_animations/icon.png"))

func _exit_tree():
    # Clean-up of the plugin goes here
    # Always remember to remove it from the engine when deactivated
    remove_custom_type("SpawnPoint")
    remove_custom_type("BodyAnimations")
