tool
extends Position2D

export(int, "right", "neutral", "left") var face_direction = 1

func get_direction():
  return face_direction - 1
