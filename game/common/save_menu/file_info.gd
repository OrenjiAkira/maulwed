
extends ColorRect

const SLOT_SIGNATURE = "[SLOT %02d]"

onready var slot = get_node("slot")
onready var map = get_node("map")
onready var time = get_node("time")
onready var quest = get_node("quest")
onready var playtime = get_node("playtime")

func load_header(slot_idx, header):
	assert(header != null)
	slot.set_text(SLOT_SIGNATURE % slot_idx)
	map.set_text(header.current_map)
	time.set_text(header.time)
	quest.set_text(header.quest)
	playtime.set_text("Playtime: %s" % header.playtime)
	show()
