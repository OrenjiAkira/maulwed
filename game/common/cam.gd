
extends Camera2D

var map
var target

func _ready():
	limit_smoothed = true

func set_map(map):
	var top_left = map.get_node("bounds/top_left").position
	var bottom_right = map.get_node("bounds/bottom_right").position
	self.map = map
	set_limit(0, top_left.x)
	set_limit(1, top_left.y)
	set_limit(2, bottom_right.x)
	set_limit(3, bottom_right.y)

func set_focus(body):
	target = body

func floor_position():
	set_global_position(get_global_position().floor())
	offset_h = round(offset_h)
	offset_v = round(offset_v)
	offset = offset.floor()

func _physics_process(delta):
	if target:
		set_global_position(target.get_global_position())
		floor_position()
