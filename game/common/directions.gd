
extends Node

const NAMES = ["DIR_UP", "DIR_DOWN", "DIR_LEFT", "DIR_RIGHT"]
const DIR_UP = Vector2(0, -1)
const DIR_DOWN = Vector2(0, 1)
const DIR_LEFT = Vector2(-1, 0)
const DIR_RIGHT = Vector2(1, 0)

