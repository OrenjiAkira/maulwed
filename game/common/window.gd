tool
extends TextureRect

func _ready():
	material.set_shader_param("DIM", rect_size)
	var corner = material.get_shader_param("CORNER")
	$shadow.rect_size = rect_size
	$shadow.material.set_shader_param("DIM", rect_size)
	$shadow.material.set_shader_param("CORNER", corner)
