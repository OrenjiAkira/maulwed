
extends CollisionPolygon2D

func _ready():
	var points = get_polygon()
	var shape = ConvexPolygonShape2D.new()
	shape.set_points(points)
	get_parent().add_shape(shape)

