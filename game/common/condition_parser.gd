
extends Object

class EQ:
	static func check(a, b):
		return a == b

class NE:
	static func check(a, b):
		return a != b

class GT:
	static func check(a, b):
		return a > b

class LT:
	static func check(a, b):
		return a < b

class GE:
	static func check(a, b):
		return a >= b

class LE:
	static func check(a, b):
		return a <= b

class Literal:
	var value
	func _init(v):
		value = v
	func get_value():
		return value

class Flag:
	var gamedata
	var flag_type
	var flag_name
	func _init(data, type, name):
		gamedata = data
		flag_type = type
		flag_name = name
	func get_value():
		if flag_type == 0:
			return gamedata.get_state(flag_name)
		elif flag_type == 1:
			return gamedata.get_persist(flag_name)

class ConditionChecker:
	var operator
	var lhs
	var rhs
	func _init(op, fl, val):
		operator = op
		lhs = fl
		rhs = val
	func check():
		return operator.check(lhs.get_value(), rhs.get_value())

const FLAG_TYPE = {"st":0, "pe":1}

static func parse(conditions_array, gamedata):
	var operators = { "==":EQ, "!=":NE, ">":GT, "<":LT, ">=":GE, "<=":LE, }
	var parsed = []
	if conditions_array == null:
		return parsed
	for raw_expr in conditions_array:
		if raw_expr.length() > 3:
			var expr = raw_expr.split(" ")
			assert(expr.size() == 3)
			var lhs_info = expr[0].split(".")
			var lhs_type = FLAG_TYPE[lhs_info[0]]
			var lhs = Flag.new(gamedata, lhs_type, lhs_info[1])
			var op = operators[expr[1]]
			var rhs
			if expr[2].is_valid_integer():
				rhs = Literal.new(expr[2].to_int())
			else:
				var info = expr[1].split(".")
				var type = FLAG_TYPE[info[0]]
				rhs = Flag.new(gamedata, type, info[1])
			var cond = ConditionChecker.new(op, lhs, rhs)
			parsed.append(cond)
	return parsed
