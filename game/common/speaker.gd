
extends Node2D

const DEFS = preload("res://domains/definitions.gd")

onready var box = get_node("name_box")
onready var label = get_node("name_box/name_text")

func set_name(name_text):
	var name_color = DEFS.CHARA.Default
	if DEFS.CHARA.has(name_text):
		name_color = DEFS.CHARA[name_text]
	box.set_self_modulate(name_color)
	label.set_text(name_text)

