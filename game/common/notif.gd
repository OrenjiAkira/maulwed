
extends Node2D

const TRANSP = Color(0xffffff00)
const OPAQUE = Color(0xffffffff)
const FADE_TIME = 0.2

enum { INTERACT_NOT, INTERACT_ACTION, INTERACT_TALK, INTERACT_EXIT }

onready var action = get_node("balloons/action")
onready var talk = get_node("balloons/talk")
onready var exit = get_node("balloons/exit")
onready var tween = get_node("tween")

var displaying = false

func _ready():
	talk.hide()
	action.hide()
	exit.hide()
	modulate = TRANSP

func set_type(type):
	if type == INTERACT_TALK:
		talk.show()
	elif type == INTERACT_ACTION:
		action.show()
	elif type == INTERACT_EXIT:
		exit.show()

func set_display(enable):
	if enable != displaying:
		displaying = enable
		tween.stop_all()
		if enable:
			tween.interpolate_property(self, "modulate", modulate, OPAQUE, FADE_TIME,
					Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
		else:
			tween.interpolate_property(self, "modulate", modulate, TRANSP, FADE_TIME,
					Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
		tween.start()

