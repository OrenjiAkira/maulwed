
extends Node2D

var map
var hud

# note: add queue for scenes for when they trigger in sequence

func _enter_tree():
	add_to_group("scene_containers")

func _exit_tree():
	remove_from_group("scene_containers")

func setup(map, hud):
	self.map = map
	self.hud = hud

func is_map_busy():
	return map.is_paused() or map.is_locked()

func play_scene(scene):
	map.pause()
	scene.connect("execution_done", self, "end_scene", [scene], CONNECT_ONESHOT)
	scene.run(map, hud)

func end_scene(scene):
	scene.end_scene()
	map.run()

func can_trigger_interact():
	for scene in get_children():
		if scene.is_trigger_by_interaction() and scene.are_conditions_met():
			return true
	return false

func trigger_interact():
	if is_map_busy():
		return false
	for scene in get_children():
		if scene.is_trigger_by_interaction() and scene.are_conditions_met():
			print("> playing trigger scene: %s" % scene.name)
			play_scene(scene)
			return true
	return false

func trigger_step():
	if is_map_busy():
		return false
	for scene in get_children():
		if scene.is_trigger_by_step() and scene.are_conditions_met():
			print("> playing step scene: %s" % scene.name)
			play_scene(scene)
			return true
	return false

func trigger_auto():
	if is_map_busy():
		return false
	for scene in get_children():
		if scene.is_trigger_by_auto() and scene.are_conditions_met():
			print("> playing auto scene: %s" % scene.name)
			play_scene(scene)
			return true
	return false

func trigger_by_name(scene_name):
	if is_map_busy():
		return false
	for scene in get_children():
		if scene.name == scene_name and scene.are_conditions_met():
			print("> playing scene: %s" % scene.name)
			play_scene(scene)
			return true
	return false

# Save & Load State

func save():
	var to = {}
	for scene in get_children():
		to[scene.name] = scene.save()
	return to

func load(from):
	for scene_name in from.keys():
		var scene_state = from[scene_name]
		print("loading scene state: ", scene_name)
		get_node(scene_name).load(scene_state)

