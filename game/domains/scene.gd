
extends "res://domains/conditional_component.gd"

export(bool) var on_interact = false
export(bool) var on_step = false
export(bool) var on_auto = false
export(bool) var one_time_only = false

var unplayable = false

# Setup and End

func end_scene():
	if one_time_only:
		unplayable = true

# Getters

func is_trigger_by_interaction():
	return on_interact

func is_trigger_by_step():
	return on_step

func is_trigger_by_auto():
	return on_auto

func are_conditions_met():
	if unplayable or progress >= 0:
		return false
	return .are_conditions_met()

# Save & Load State

func save():
	print("saving scene: ", name)
	var to = {}
	to.unplayable = unplayable
	return to

func load(from):
	print("loading scene: ", name)
	unplayable = (from.unplayable == true)

