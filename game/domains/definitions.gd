
extends Object

const DEFAULT_GAME_SPEED = 1
const SECOND_PER_SECOND = 90.0

const STORY = [
		"Prologue", 						# 0. The Day Before
		"Prelude",							# 1. During the afternoon
		"Ch: Odd One Out", 			# 2. First Cycle Investigation
		"Ch: Play the Play",		# 3. Rick's Play at Lover's Place
		"Ch: Moment's Memory",	# 4. Stay at Hasha's, talk about memories
		"Ch: Trash Talk",				# 5. Ty and the lovers hide in the trash from coyote
		"Ch: Silent Sadness",		# 6. Conclusion
		"Epilogue" ]						# 7. Thursday

const CHARA = {
		"Tyche": Color(0x306082ff),
		"Hasha": Color(0xa5550fff),
		"Rick": Color(0x3c8242ff),
		"Derek": Color(0x8f713dff),
		"Default": Color(0x674a4aff), }

enum PLACES {
	TYCHE_APARTMENT,			#0
	HASHA_SHOP,						#1
	TALON_AV,							#2
	WOOL_ST,							#3
	ELIS_HYENA_PARK,			#4
	DEERS_BAR,						#5
	BOYFRIENDS_APARTMENT,	#6
	MARKET,								#7
}
