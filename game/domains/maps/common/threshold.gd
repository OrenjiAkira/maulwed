extends Area2D

const TRANSP = Color(0xffffff00)
const OPAQUE = Color(0xffffffff)
const FADE_TIME = 0.25

onready var arrow = get_node("arrow")
onready var tween = get_node("tween")
onready var map = get_tree().get_nodes_in_group("map")[0]

var displaying = false
var waiting_for_unpause = false

func _ready():
	arrow.modulate = TRANSP
	connect("area_entered", self, "show_arrow")
	connect("area_exited", self, "hide_arrow")

func _physics_process(dt):
	if displaying and map.is_paused() and not waiting_for_unpause:
		waiting_for_unpause = true
		fade_out()
	elif displaying and not map.is_paused() and waiting_for_unpause:
		waiting_for_unpause = false
		fade_in()

func show_arrow(area):
	displaying = true
	fade_in()

func hide_arrow(area):
	displaying = false
	fade_out()

func fade_in():
	tween.stop_all()
	tween.interpolate_property(arrow, "modulate",
			arrow.modulate, OPAQUE, FADE_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()

func fade_out():
	tween.stop_all()
	tween.interpolate_property(arrow, "modulate",
			arrow.modulate, TRANSP, FADE_TIME,
			Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()

