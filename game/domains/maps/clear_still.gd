
extends "res://domains/component.gd"

export(float, EXP, 0, 256, 0.1) var fade_time = 1

func run(map, hud):
	var still_texture = hud.still_texture
	still_texture.clear_still(fade_time)
	yield(still_texture, "clear_done")
	emit_signal("execution_done")
