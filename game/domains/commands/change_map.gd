
extends "res://domains/component.gd"

export(String) var map_id = ""
export(String) var spawn_point = ""

func run(map, hud):
	map.set_next_map(map_id, spawn_point)
	emit_signal("execution_done")

