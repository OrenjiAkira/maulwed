
extends "res://domains/component.gd"

const EPSILON = 8.0

export(String) var body_name = ""
export(bool) var wait = true

onready var target = get_node("target")

var body


func _ready():
	set_physics_process(false)

func _physics_process(dt):
	var how_far = target.get_global_position().x - body.get_global_position().x
	if abs(how_far) < EPSILON:
		set_physics_process(false)
		if wait:
			emit_signal("execution_done")
	else:
		if how_far < 0:
			body.walk(body.DIR_LEFT)
		else:
			body.walk(body.DIR_RIGHT)			


func run(map, hud):
	assert(target)
	assert(map.has_body(body_name))
	body = map.get_body(body_name)
	set_physics_process(true)
	if not wait:
		emit_signal("execution_done")

