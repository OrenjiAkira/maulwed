
extends "res://domains/component.gd"

export(AudioStream) var sfx
export(bool) var wait = false
export(bool) var spatial = false

onready var se_player = get_node("se_player")
onready var se_player2D = get_node("se_player2D")

func _ready():
	assert(sfx)
	se_player.set_stream(sfx)
	se_player2D.set_stream(sfx)

func run(map, hud):
	if spatial:
		se_player2D.play()
		if wait:
			yield(se_player2D, "finished")
	else:
		se_player.play()
		if wait:
			yield(se_player, "finished")
	emit_signal("execution_done")

