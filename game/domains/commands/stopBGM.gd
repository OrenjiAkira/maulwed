
extends "res://domains/component.gd"

export(float, EXP, 0.1, 10, 0.1) var fade_time = 1

onready var bgm = get_node("/root/music")

func run(map, hud):
	bgm.stop(fade_time)
	map.is_bgm_playing = false
	emit_signal("execution_done")

