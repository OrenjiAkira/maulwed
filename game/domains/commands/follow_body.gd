
extends "res://domains/component.gd"

export(String) var stalker = ""
export(String) var stalkee = ""
var stalker_body
var stalkee_body

func run(map, hud):
	assert(map.has_body(stalker))
	assert(map.has_body(stalkee))
	stalker_body = map.get_body(stalker)
	stalkee_body = map.get_body(stalkee)
	stalker_body.set_global_position(stalkee_body.get_global_position())
	yield(get_tree(), "physics_frame")
	emit_signal("execution_done")
