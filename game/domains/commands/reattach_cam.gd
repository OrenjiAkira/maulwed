
extends "res://domains/component.gd"

func run(map, hud):
	var camera = map.camera # player camera
	var mapcam = map.mapcam # map camera
	var campos = mapcam.get_global_position()
	camera.set_global_position(campos)
	camera.make_current()
	emit_signal("execution_done")
