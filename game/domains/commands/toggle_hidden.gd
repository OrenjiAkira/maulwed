
extends "res://domains/component.gd"

export(String) var body_name = ""
export(bool) var make_hidden = false

func run(map, hud):
	assert(map.has_body(body_name))
	var body = map.get_body(body_name)
	body.set_hidden(make_hidden)
	emit_signal("execution_done")
