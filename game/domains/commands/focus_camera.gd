
extends "res://domains/component.gd"

export(float, EXP, 0.0, 10, 0.1) var time = 1.0
export(String) var body_name = ""

var timer

func _enter_tree():
	timer = Timer.new()
	add_child(timer)

func _exit_tree():
	timer.free()

func run(map, hud):
	assert(map.has_body(body_name))
	var target = map.get_body(body_name)
	map.camera.set_focus(target)
	if time > 0:
		timer.set_wait_time(time)
		timer.start()
		yield(timer, "timeout")
	emit_signal("execution_done")

