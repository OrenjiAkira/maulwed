
extends "res://domains/conditional_component.gd"

export(String) var if_true
export(String) var if_false

# Setup and Execution

func run(map, hud):
	if are_conditions_met():
		play_scene(if_true, map, hud)
	else:
		play_scene(if_false, map, hud)

func play_scene(scene_path, map, hud):
	var scene = get_scene(scene_path)
	if scene:
		scene.connect("execution_done", self, "emit_signal",
				["execution_done"], CONNECT_ONESHOT)
		scene.run(map, hud)
	else:
		emit_signal("execution_done")

# Checkers and Getters

func get_scene(scene_path):
	if scene_path == null or scene_path == "" or !has_node(scene_path):
		return false
	else:
		return get_node(scene_path)

