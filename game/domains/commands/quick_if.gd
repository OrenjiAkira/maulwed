
extends "res://domains/conditional_component.gd"

func run(map, hud):
  if are_conditions_met():
    .run(map, hud)
  else:
    emit_signal("execution_done")
