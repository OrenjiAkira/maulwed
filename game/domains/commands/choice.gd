
extends "res://domains/component.gd"

export(String, MULTILINE) var question = ""
export(String, MULTILINE) var option0 = ""
export(String) var option0_target = ""
export(String, MULTILINE) var option1 = ""
export(String) var option1_target = ""

onready var se = get_node("/root/se")

var selection = 0

var choice_box
var map
var hud

# Specials

func _ready():
	set_process_input(false)
	assert(!option0.empty())
	assert(!option1.empty())

func _input(ev):
	if ev.is_action_pressed("DIR_UP") or ev.is_action_pressed("DIR_DOWN"):
		se.play("toggle")
		selection = selection ^ 1
		choice_box.set_selection(selection)
	elif ev.is_action_pressed("ACTION_A"):
		se.play("confirm")
		confirm()


# Setup

func run(map, hud):
	self.map = map
	self.hud = hud
	selection = 0
	choice_box = hud.choice_box
	choice_box.setup(question, [option0, option1])
	yield(choice_box, "choice_entered")
	set_process_input(true)

func confirm():
	set_process_input(false)
	choice_box.stop()
	yield(choice_box, "choice_left")
	match selection:
		0: play_scene(option0_target)
		1: play_scene(option1_target)


# Execution

func play_scene(scene_path):
	var scene = get_scene(scene_path)
	if scene == null:
		emit_signal("execution_done")
	else:
		scene.connect("execution_done", self, "emit_signal",
				["execution_done"], CONNECT_ONESHOT)
		scene.run(map, hud)

# Getters and Checkers

func get_scene(scene_path):
	if scene_path.empty() or !has_node(scene_path):
		return null
	else:
		return get_node(scene_path)


