
extends "res://domains/component.gd"

export(AudioStream) var audio_stream
export(float, EXP, 0.1, 10, 0.1) var fade_time = 1
export(float, EXP, 0, 360, 0.1) var seek_time = 0

onready var bgm = get_node("/root/music")

func run(map, hud):
	bgm.play(audio_stream, fade_time, seek_time)
	emit_signal("execution_done")

