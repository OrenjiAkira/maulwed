
extends "res://domains/component.gd"

export(String) var stil_name = ""
export(float, EXP, 0, 256, 0.1) var fade_time = 1

func run(map, hud):
	var still_texture = hud.still_texture
	still_texture.set_still(stil_name, fade_time)
	yield(still_texture, "fade_done")
	emit_signal("execution_done")
