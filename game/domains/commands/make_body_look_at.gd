
extends "res://domains/component.gd"

enum { AT_LEFT, AT_RIGHT, AT_BODY }

export(String) var body_name = ""
export(int, "left", "right", "body") var direction = 0
export(String) var target_body_name = ""

func run(map, hud):
	assert(map.has_body(body_name))
	var body = map.get_body(body_name)
	match direction:
		AT_LEFT:
			body.set_flip(body.appearance.FLIP_LEFT)
		AT_RIGHT:
			body.set_flip(body.appearance.FLIP_RIGHT)
		AT_BODY:
			assert(map.has_body(target_body_name))
			var target_body = map.get_body(target_body_name)
			body.look_at(target_body)
	emit_signal("execution_done")
