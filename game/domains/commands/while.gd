
extends "res://domains/commands/quick_if.gd"

func stop(map, hud):
	print("ENDWHILE")
	if are_conditions_met():
		# while true
		var components = get_children()
		assert(components.size() > 0)
		# execute block if block exists
		yield(get_tree(), "physics_frame")
		run(map, hud)
	else:
		# break, finish command execution
		.stop(map, hud)

func _exit_tree():
	var tree = get_tree()
	for connection in tree.get_signal_connection_list("physics_frame"):
		print("CONNECTION: %s" % connection)

