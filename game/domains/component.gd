
extends Node2D

signal execution_done

var progress = -1
var processes = []
var paused = false
var continuation

func _ready():
	stop_processes()

func run(map, hud):
	progress = 0
	continuation = next_component(map, hud)

func next_component(map, hud):
	while is_paused():
		yield()
	var components = get_children()
	if progress >= 0 and progress < components.size():
		var comp = components[progress]
		comp.connect("execution_done", self, "end_component",
				[map, hud], CONNECT_ONESHOT)
		comp.run(map, hud)
	else:
		stop(map, hud)

func stop(map, hud):
	progress = -1
	emit_signal("execution_done")

func end_component(map, hud):
	progress += 1
	continuation = next_component(map, hud)

func pause():
	print("pausing scene %s" % get_name())
	paused = true
	stop_processes()
	var components = get_children()
	if progress >= 0 and progress < components.size():
		var comp = components[progress]
		comp.pause()

func stop_processes():
	processes.clear()
	if is_processing():
		processes.append("set_process")
		set_process(false)
	if is_physics_processing():
		processes.append("set_physics_process")
		set_physics_process(false)
	if is_processing_input():
		processes.append("set_process_input")
		set_process_input(false)

func play_processes():
	for process in processes:
		call(process, true)
	processes.clear()

func resume():
	print("resuming scene %s" % get_name())
	paused = false
	play_processes()
	if continuation:
		continuation.resume()
	var components = get_children()
	if progress >= 0 and progress < components.size():
		var comp = components[progress]
		comp.resume()

func is_paused():
	return paused

