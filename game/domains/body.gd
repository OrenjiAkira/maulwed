
extends KinematicBody2D

const MOVEMENT_SIZE = 32
const SPEED_UNIT = 1.0
const BASE_SPEED = 4.0

enum { DIR_NEUTRAL = 0, DIR_LEFT = -1, DIR_RIGHT = 1 }
enum { SPEED_SLOW, SPEED_MID, SPEED_FAST }
enum { STATE_IDLE, STATE_WALK, STATE_RUN }

export(bool) var hidden = false
export(bool) var locked_animation = false
export(int, "idle", "walk", "run") var default_state = 0
export(int, "left", "right") var default_flipped = 0
export(int, "not", "action", "talk", "exit") var interactable = 0
export(int, "slow", "mid", "fast") var speed = SPEED_SLOW

onready var appearance = get_node("appearance")
onready var interaction_area = get_node("interaction_area")
onready var notif = get_node("notif")

var current_sprite
var has_init = false
var movement = Vector2(0, 0)
var moving = DIR_NEUTRAL

func _enter_tree():
	add_to_group("bodies")

func _exit_tree():
	remove_from_group("bodies")

func _ready():
	if !has_init:
		visible = not hidden
		if default_flipped == 0:
			default_flipped = 1
		elif default_flipped == 1:
			default_flipped = -1
		set_flip(default_flipped)
		match default_state:
			STATE_IDLE:
				set_idle_state()
			STATE_WALK:
				set_walk_state()
			STATE_RUN:
				set_run_state()
		notif.set_type(interactable)
		has_init = true

func _physics_process(dt):
	execute_movement(dt)
	if !locked_animation:
		match moving:
			DIR_RIGHT:
				if appearance.get_flip() != appearance.FLIP_RIGHT:
					set_flip(appearance.FLIP_RIGHT)
			DIR_LEFT:
				if appearance.get_flip() != appearance.FLIP_LEFT:
					set_flip(appearance.FLIP_LEFT)
		if appearance.get_state() == appearance.STATE_IDLE and is_moving():
			set_moving_state()
		elif (appearance.get_state() == appearance.STATE_WALK or \
				 appearance.get_state() == appearance.STATE_RUN) and not is_moving():
			set_idle_state()
	moving = DIR_NEUTRAL

# Scene-related

func show_notification():
	notif.set_display(true)

func hide_notification():
	notif.set_display(false)

func has_scenes():
	return has_node("scenes")

func get_scenes():
	if has_scenes():
		return get_node("scenes")

func has_interactable_scenes():
	return has_scenes() and get_scenes().can_trigger_interact()

func get_interaction_area():
	return interaction_area

# Movement

func set_speed(spd):
	speed = spd

func get_speed():
	return BASE_SPEED + SPEED_UNIT * speed

func limit_speed():
	var max_speed = get_speed()
	if abs(movement.x) > max_speed:
		movement.x = get_direction() * max_speed

func walk(direction):
	moving = direction

func stop_moving():
	moving = DIR_NEUTRAL
	movement.x = 0

func is_moving():
	return moving != DIR_NEUTRAL

func get_direction():
	return moving

func execute_movement(dt):
	if not is_moving():
		stop_moving()
	# fullspeed in 1/6 of a second, 1/3 if changing direction
	movement.x += get_direction() * get_speed() * dt * 6
	limit_speed()
	move_and_slide(movement * MOVEMENT_SIZE, Vector2(0, -1))


# Appearance

func set_hidden(is_hidden):
	hidden = is_hidden
	visible = not hidden

func is_hidden():
	return hidden

func set_sprite(sprite_name):
	if sprite_name:
		current_sprite = sprite_name
		appearance.set_sprite(sprite_name)

func set_flip(flip):
	if flip == 0:
		return
	appearance.set_flip(flip)

func set_idle_state():
	appearance.set_idle_state()

func set_walk_state():
	appearance.set_walk_state()

func set_run_state():
	appearance.set_run_state()

func set_animation(animation_name):
	appearance.play_custom(animation_name)

func set_moving_state():
	if speed >= SPEED_FAST:
		set_run_state()
	else:
		set_walk_state()

func look_at(target):
	var distvec = target.get_global_position() - get_global_position()
	if distvec.x < 0:
		set_flip(appearance.FLIP_LEFT)
	elif distvec.x > 0:
		set_flip(appearance.FLIP_RIGHT)

func is_animation_locked():
	return locked_animation

func lock_animation():
	locked_animation = true

func unlock_animation():
	locked_animation = false

# Save & Load State

func save():
	print("saving body: ", name)
	var to = {}
	to.position = [position.x, position.y]
	to.appearance = appearance.save()
	to.hidden = hidden
	to.speed = speed
	if current_sprite:
		to.current_sprite = current_sprite
	if has_scenes():
		to.scenes = get_scenes().save()
	return to

func load(from):
	print("loading body: ", name)
	position = Vector2(from.position[0], from.position[1])
	appearance.load(from.appearance)
	set_hidden(from.hidden)
	set_speed(from.speed)
	if from.has("current_sprite"):
		set_sprite(from.currrent_sprite)
	if has_scenes() and from.has('scenes'):
		get_scenes().load(from.scenes)
