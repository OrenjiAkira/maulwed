
extends Node2D

var map
var hud
var playing = []

func setup(map, hud):
	self.map = map
	self.hud = hud

func pause():
	for behaviour in playing:
		behaviour.pause()

func resume():
	for behaviour in playing:
		behaviour.resume()

func check():
	for behaviour in get_children():
		if not (behaviour in playing) and behaviour.are_conditions_met():
			playing.append(behaviour)
			behaviour.connect("execution_done", self, "finish_behaviour",
					[behaviour], CONNECT_ONESHOT)
			behaviour.run(map, hud)

func finish_behaviour(behaviour):
	var idx = playing.find(behaviour)
	playing.remove(idx)
