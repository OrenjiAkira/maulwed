
extends AnimationPlayer

func animation_changed(old_name, new_name):
	if new_name == "smile":
		play("wag")
	elif old_name == "smile":
		play("idle")
