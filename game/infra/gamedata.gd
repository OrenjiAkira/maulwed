
extends Node

const DEFS = preload("res://domains/definitions.gd")
const ENCODE = preload("res://infra/encode.gd")
const TIME = preload("res://states/gameplay/hud/time_display.gd")

# Static Methods

static func delete(slot):
	return ENCODE.delete_file(slot)

signal game_saving

var id
var playtime = 0
var persist = { "cycles":0, "endings":0, "quest":0, "game_speed":DEFS.DEFAULT_GAME_SPEED }
var state = { "time":0 }
var current_map = { "map_id":null, "map_name":null, "spawn":null }
var maps = {}
var player_data = {}

# Initialisation

func _init(raw_id):
	set_pause_mode(PAUSE_MODE_PROCESS)
	id = raw_id

func _process(dt):
	playtime += dt

func reset():
	state.clear()
	maps.clear()

func load(slot):
	var data = ENCODE.load_from(slot)
	if !data:
		return false
	id = data.id
	playtime = data.playtime
	persist = data.persist
	state = data.state
	maps = data.maps
	current_map = data.current_map
	player_data = data.player_data
	return true

func save(slot):
	emit_signal("game_saving")
	var data = {
		"id": id,
		"playtime": playtime,
		"persist": persist,
		"state": state,
		"maps": maps,
		"current_map": current_map,
		"player_data": player_data }
	if ENCODE.save_to(slot, data):
		return true
	return false

# Setters and Getters

func get_id():
	return id

func get_playtime():
	var total = playtime
	var cs = floor((total - floor(total)) * 100)
	var s = floor(fmod(total, 60))
	var m = floor(total / 60)
	var h = floor(total / 3600)
	return "%02d:%02d:%02d:%02d" % [h, m, s, cs]

func get_header():
	return {
			"playtime": get_playtime(),
			"current_map": current_map.map_name,
			"quest": DEFS.STORY[persist.quest],
			"time": TIME.parse(state.time) }

func set_state(key, value):
	state[key] = int(value)

func get_state(key):
	if !state.has(key):
		state[key] = 0
	return int(state[key])

func set_persist(key, value):
	persist[key] = int(value)

func get_persist(key):
	if !persist.has(key):
		persist[key] = 0
	return int(persist[key])

# Map Stuff

func set_current_map(map_id, map_name, spawn):
	current_map.map_id = map_id
	current_map.map_name = map_name
	current_map.spawn = spawn

func get_current_map():
	return current_map.duplicate()

func save_map(map_id, map):
	var map_data = map.save()
	maps[map_id] = map_data

func load_map(map_id, map):
	if maps.has(map_id):
		var map_data = maps[map_id]
		map.load(map_data)

# Player Data

func load_player_data(player):
	if !player_data.empty():
		player.load(player_data)

func save_player_data(player):
	player_data = player.save()
