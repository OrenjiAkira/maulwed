
extends Node

func play(sfx_name):
	get_node(sfx_name).play()

func is_valid_sfx(sfx_name):
	return has_node(sfx_name)

