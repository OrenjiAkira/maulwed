
extends Node

const AUDIBLE = 0
const INAUDIBLE = -80

onready var tween_start = get_node("tween_start")
onready var tween_stop = get_node("tween_stop")
onready var buffers = get_node("buffers").get_children()

var next_idx = 0

# Actions

func play(audio_stream, s, from = 0):
	var prev = get_prev()
	var next = get_next()
	if prev.is_playing():
		if prev.get_stream() == audio_stream:
			return
		else:
			stop_playing(prev, s)
	next.set_stream(audio_stream)
	start_playing(next, s, from)
	toggle_focus()

func stop(s):
	var current = get_prev()
	var next = get_next()
	if current.is_playing():
		stop_playing(current, s)
	if next.is_playing():
		stop_playing(next, s)

# Cross Fading

func start_playing(player, s, from):
	player.play()
	player.seek(from)
	if s > 0:
		player.volume_db = INAUDIBLE
		tween_start.stop_all()
		tween_start.interpolate_property(player, "volume_db", INAUDIBLE, AUDIBLE, s,
				Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
		tween_start.start()

func stop_playing(player, s):
	if s > 0:
		tween_stop.stop_all()
		tween_stop.interpolate_property(player, "volume_db", AUDIBLE, INAUDIBLE, s,
				Tween.TRANS_QUAD, Tween.EASE_IN, 0)
		tween_stop.start()
		yield(tween_stop, "tween_completed")
	player.stop()

func toggle_focus():
	next_idx = (next_idx + 1) % 2

# Getters

func get_prev():
	return buffers[(next_idx + 1) % 2]

func get_next():
	return buffers[next_idx]

