# PROFILE
extends Node

const GAMEDATA = preload("res://infra/gamedata.gd")
const ENCODE = preload("res://infra/encode.gd")

const PROFILE_FILENAME = "profile"
const SLOT_NAME_SIGNATURE = "file%02d"
const AVAILABLE_SLOTS = 16

# meta static function
static func get_slot_name(slot_idx):
	assert(slot_idx >= 0 and slot_idx < AVAILABLE_SLOTS)
	return SLOT_NAME_SIGNATURE % slot_idx


var next_id = 0
var save_list = {}
var preferences = {}
var last_save = false
var current_save


# Private Initialisation & Profile Saving/Loading

func _ready():
	assert(load_profile())

func _exit_tree():
	save_profile()

func load_profile():
	var data = ENCODE.load_from(PROFILE_FILENAME)
	if data:
		next_id = data.next_id
		save_list = data.save_list
		last_save = data.last_save
		preferences = data.preferences
	return save_profile()

func save_profile():
	var data = { "next_id":next_id, "save_list":save_list,
			"last_save":last_save, "preferences":preferences }
	var success = ENCODE.save_to(PROFILE_FILENAME, data)
	return success

func has_saves():
	return !save_list.empty()

# Private Setting Methods

func set_current_save(savedata):
	add_child(savedata)
	current_save = savedata

func unset_current_save():
	if current_save:
		current_save.queue_free()
		current_save = null

func load_game(slot):
	var gamedata = GAMEDATA.new(0)
	gamedata.load(slot)
	if !gamedata:
		return false
	last_save = slot
	save_profile()
	unset_current_save()
	set_current_save(gamedata)
	return true

func delete_game(slot):
	save_list.erase(slot)
	return save_profile() and GAMEDATA.delete(slot)

# Public Start New Game Method

func new_game():
	var gamedata = GAMEDATA.new(next_id)
	next_id += 1
	set_current_save(gamedata)


# Public Saving/Loading/Deleting Methods

func load_game_from_slot(slot_idx):
	var slot = get_slot_name(slot_idx)
	return load_game(slot)

func save_game_to_slot(slot_idx):
	var slot = get_slot_name(slot_idx)
	return current_save.save(slot) and save_game_header(slot, current_save)

func delete_game_in_slot(slot_idx):
	var slot = get_slot_name(slot_idx)
	return delete_game(slot)


# Header Info Methods

func save_game_header(slot, save_data):
	last_save = slot
	save_list[slot] = save_data.get_header()
	return save_profile()

func get_slot_info(slot):
	if save_list.has(slot):
		return save_list[slot]
	return false

func get_last_slot_index():
	if not profile.last_save:
		return -1
	for idx in range(profile.AVAILABLE_SLOTS):
		var save_name = profile.SLOT_NAME_SIGNATURE % idx
		if profile.last_save == save_name:
			return idx
	return -1

# Preferences Setters & Getters

func get_preference(field):
	if preferences.has(field):
		return preferences[field]

func set_preference(field, value):
	preferences[field] = value
