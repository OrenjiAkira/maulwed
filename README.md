
# Mauling Wednesday

A furry game about a young adult lynx who becomes trapped in a timeloop.

# How to play

[Download the latest release here](https://drive.google.com/open?id=1_kQTFYyE89X03amHPKEMCVX1IYZJFHRO).
They are named according to version/OS.

Or, if you prefer, clone this repository and import it
with [Godot Engine](https://godotengine.org/).

Controls are as follow:

```
[directionals] -> movement
[z] -> confirm
[x] -> cancel/skip text
[c] -> fast-forward text
[escape] -> save & quit
```

### Disclaimer

Contains swearing, violence, and mentions of sexual themes.
