
[Tyche]
But there's no novelty here.

We grew up in this neighbourhood.

Although it has changed a bit,
everything is still sort of the same.

It's hard to feel something about it.

We're too close to it."

[Hasha]
I guess.

But I don't know, I like being here sometimes.

Feel the sun on my fur...

Although it's already setting now.

* pause wait *

[Tyche]
Hey, uh...

What did that girl mean about
Ms. Rattington's son?

[Hasha]
Oh, Dave.
You weren't around, so you probably didn't hear.

He was...

...Mauled.

[Tyche]
...What?

Who the hell mauls people to death?

[Hasha]
It was a lion apparently.

But when they captured him,
he was all crazy.

Like, he attacked everyone,
ran on all fours.

Kind of like that coyote you saw,
now that I think of it.

It was all over the news too,
how did you not see that?

[Tyche]
...I was busy with homework?

Also television through
streaming is a thing.

[Hasha]
.....

Hey, this is a little going on a tangent, but...

Did you know this park has like an urban legend?

[Tyche]
You don't say.

Is it about a ghost?

[Hasha]
Pfft, ghosts.

Those only haunt small towns and abandoned stuff.

No, it's more of fae thing?

[Tyche]
...Fae?

[Hasha]
Yeah, like...

I don't wanna say faeries
because it sounds lame.

But like, an entity of sorts.

[Tyche]
Okay...

And what, it scolds litterers?

[Hasha]
No! _She_, they say it's a she.

[Tyche]
And apparently there is a they too.

[Hasha]
Hush! Let me tell it!

Anyway, _she_ gives bad luck
to those who wrong her.

[Tyche]
...That's it?

And, what, pray tell,
does it mean to wrong her?

[Hasha]
I heard she likes young women.

Like, she protects young women
from assault and stuff.

[Tyche]
That sounds like bullshit.

I've been harrassed plenty
times in here.

Especially late at night after-

* A W O O *
* song stop *
* fade back to park, it's night *

[Hasha]
Did I just hear...?

[Tyche]
.....

Yeah, definitely.

[Hasha]
It came from over there, come on!

[Tyche]
What!? We're chasing it?

* fade to woolst van, tense music *

[Mane]
Ugh, get in!

[Coyote]
GGRRR

[Mane]
Damn it!

* pan to tyche and hasha *

[Mane]
Shit.

[Hasha]
Oh god.

[Tyche]
Hash, come on, let's go!

[Hasha]
.....

Hey, what're you doing with this guy?

What's wrong with him?

[Mane]
Fuck.

Fuck. Fuck. Fuck!

* kills coyote? *

[Tyche]
Oh my god.

[Hasha]
Dude, what the hell?

You fucking killed him!?

[Mane]
You shouldn't be here!

* black suddenly *

* sound of lunging at hasha, music stop *

[Hasha]
AAAH!

[Tyche]
No!

* wait *

[Tyche]
Oh no, please no, not Hash.

I can't lose her, not her, please.

Anyone but her.

* flash to ty's mortified face *

Stop this. This isn't real.

It's fake it's all fake.

It's a bad dream.

Just a dream.

Not real.

Please.

I can't do anything without her.

Move, Tyche.

Do something!

* back to action *
* hasha is holding her head, mane is on the floor *

[Hasha]
GGGRRRR

[Mane]
W-what!? You too!?

* TF sequence *

[Mane]
W-wait!

* hasha pounces, fade to black, mauling noises *

* Wild hasha with blood dripping from her muzzle *

* tyche on floor *

[Hasha] roarsfx
.....

[Tyche]
.....

...What is happening!?

...Hash...?

Are you... OK?

.....

Hash...
...Please.....

* pounce, instant black, roar, scream *


